#include <iostream>
#include "png++/png.hpp"
#include "mlp.hpp"

using namespace std;
using namespace png;

int main ( int argc, char *argv[] )
{
	image <rgb_pixel> png_image (argv[1]);
	image <rgb_pixel> new_png_image(png_image.get_width(), png_image.get_height());
	int size = png_image.get_width() * png_image.get_height();

	Perceptron* p = new Perceptron(3, size, 256, size);
	
	double* image = new double[size];

	for (int i{0}; i < png_image.get_width(); i++)
		for (int j{0}; j < png_image.get_height(); j++){
			image[i * png_image.get_width() + j] = png_image[i][j].red;
			
		}
	double* value = (*p)(image);
	for (int i = 0; i < png_image.get_width(); i++)
		for (int j = 0; j < png_image.get_height(); j++){
			new_png_image[i][j].red = png_image[i][j].red;
			new_png_image[i][j].green = png_image[i][j].green;
			new_png_image[i][j].blue = value[i * png_image.get_width() + j];
		}
	
	new_png_image.write("uj_mandel.png");
	delete p;
	delete [] image;
}
