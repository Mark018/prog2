#include <vector>
#include <iostream>
#include <cxxabi.h>

using namespace std;
template<typename T>
struct CustomAlloc {
	using size_type = size_t;
	using value_type = T;
	using pointer = T *;
	using const_pointer = const T*;
	using reference = T &;
	using const_reference = const T &;
	using difference_type = ptrdiff_t;
	CustomAlloc() {}
	CustomAlloc(const CustomAlloc&) {}
	~CustomAlloc() {}
	pointer allocate(size_type n) {
		int s;
		char* p = abi::__cxa_demangle(typeid(T).name(), 0, 0, &s);
		std::cout << "Allocating "
			<< n << " object of "
			<< n * sizeof(T)
			<< " bytes. "
			<< typeid(T).name() << " = " << p
			<< " h�v�sok sz�ma "
			<< std::endl;
		free(p);
		return reinterpret_cast<T*> (new int[n * sizeof(T)]);
	}
	void deallocate(pointer p, size_type n) {
		delete[] reinterpret_cast<int*> (p);
		std::cout << "Deallocating "
			<< n << " object of "
			<< n * sizeof(T)
			<< " bytes. "
			<< typeid(T).name() << " = " << p
			<< " h�v�sok sz�ma "
			<< std::endl;
	}
};

int main() {
	vector< int, CustomAlloc<int> > v;
	v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v.push_back(9);
	v.push_back(10);
	for (int x : v) {
		cout << x << "\n";
	}
	return 0;
}