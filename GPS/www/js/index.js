var app = {
    lastPoint: {
        lat: 0,
        lng: 0
    },
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            mapTypeId: google.maps.MapTypeId.STATELITE,
            zoom: 18
        });

        var start = new google.maps.Marker({
            position: {lat: -34.397, lng: 150.644},
            map: map,
            title: "innen indultál!",
            icon: "https://puu.sh/Ev9qB/5073e46bd9.png"
        });

        var marker = new google.maps.Marker({
            position: {lat: -34.397, lng: 150.644},
            map: map,
            title: "Itt vagy!",
            icon: "https://puu.sh/Ev9pd/da5af1dd35.png"
        });

        function onSuccess(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
            marker.setPosition(pos);

            if ((app.lastPoint.lat + app.lastPoint.lng) != 0)
                new google.maps.Polyline({
                    path: [app.lastPoint, pos],
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                    map: map
                });
            else
                start.setPosition(pos);

            app.lastPoint = pos;
        }
        
        function onError(error) {
            console.log('code: '    + error.code    + '\n' +
                  'message: ' + error.message + '\n');
        }
        
        var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { frequency: 500 });
    },
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    }
};
