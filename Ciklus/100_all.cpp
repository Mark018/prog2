#include <stdio.h>
#include <omp.h>

int main()
{
	int a = 1;
	int i;
	#pragma omp parallel for
	for (i = 0; i < 10; i++) {
			a++;
			i--;
	}
    return 0;
}
