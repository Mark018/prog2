#include "../std_lib_facilities.h"

int main()
{
	int a = 40;
    int b = 25;

    cout << "Segédváltozóval:" << endl << endl;
	
    cout << "Kiindulás:" << endl;
    cout << "A: " << a << endl;
    cout << "B: " << b << endl;

	int c = a;
	a = b;
	b = c;

    cout << "Művelet után:" << endl;
    cout << "A: " << a << endl;
    cout << "B: " << b << endl;

    cout << endl << "Segédváltozó nélkül:" << endl << endl;
	
    cout << "Kiindulás:" << endl;
    cout << "A: " << a << endl;
    cout << "B: " << b << endl;

	b = b - a;
	a = a + b;
	b = a - b;

    cout << "Művelet után:" << endl;
    cout << "A: " << a << endl;
    cout << "B: " << b << endl;
}