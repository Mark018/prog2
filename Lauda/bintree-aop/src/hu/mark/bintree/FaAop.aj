package hu.mark.bintree;

public aspect FaAop {
	public pointcut kiiras(Elem e):
		call(String Fa.kiir(Elem)) && args(e);
	
	after(Elem e): kiiras(e) {
		System.out.println("Ki�r�s ut�n...");
		
		reverseOrder(e);
	}
	
	before(Elem e): kiiras(e) {
		System.out.println("Ki�r�s el�tt...");
	}
	
	private void reverseOrder(Elem e) {
		if (e.VanJobbOldal())
			reverseOrder(e.JobbOldal());
		
		if (e.Melyseg() == 0) {
			System.out.println("---/(0)");
		} else {
			for (int i = 0; i < e.Melyseg(); i++)
				System.out.print("---");
			System.out.println((e.Oldal() ? "0" : "1") + " (" + e.Melyseg() + ")");
		}
		
		if (e.VanBalOldal())
			reverseOrder(e.BalOldal());
	}
}
