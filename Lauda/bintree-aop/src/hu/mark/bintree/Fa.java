package hu.mark.bintree;

public class Fa {
	private Elem fa;
	private Elem gyoker;
	private String ki;
	private int depth;
	private double mean;
	private double count;
	private double var;

	public Fa() {
		this.gyoker = new Elem();
		this.fa = this.gyoker;
	}

	public void UjElem(boolean oldal) {
		if (oldal) {
			if (this.fa.VanBalOldal()) {
				this.fa = this.fa.BalOldal();
			} else {
				Elem ujElem = new Elem(false, this.fa.Melyseg() + 1);
				this.fa.BalOldal(ujElem);
				this.fa = this.gyoker;
			}
		} else {
			if (this.fa.VanJobbOldal()) {
				this.fa = this.fa.JobbOldal();
			} else {
				Elem ujElem = new Elem(true, this.fa.Melyseg() + 1);
				this.fa.JobbOldal(ujElem);
				this.fa = this.gyoker;
			}
		}
	}

	private void recKiir(Elem e) {
		if (e.Melyseg() > depth)
			depth = e.Melyseg();
		if (e.VanBalOldal())
			recKiir(e.BalOldal());

		if (e.Melyseg() == 0) {
			ki += "---/(0)\n";
		} else {
			for (int i = 0; i < e.Melyseg(); i++)
				ki += ("---");
			ki += ((e.Oldal() ? "0" : "1") + " (" + e.Melyseg() + ")\n");
		}

		if (e.VanJobbOldal())
			recKiir(e.JobbOldal());
	}

	private void ratlag(Elem e) {
		if (e.VanBalOldal())
			ratlag(e.BalOldal());
		if (!e.VanBalOldal() && !e.VanJobbOldal()) {
			mean += e.Melyseg();
			count++;
		}
		if (e.VanJobbOldal())
			ratlag(e.JobbOldal());
	}

	private void rszoras(Elem e) {
		if (e.VanBalOldal())
			rszoras(e.BalOldal());
		if (!e.VanBalOldal() && !e.VanJobbOldal())
			var += Math.pow((double) e.Melyseg() - mean, 2);
		if (e.VanJobbOldal())
			rszoras(e.JobbOldal());
	}

	public int getDepth() {
		return depth;
	}
	
	public double getMean() {
		return mean;
	}
	
	public double getVar() {
		return var;
	}
	
	public String kiir(Elem innen) {
		depth = 0;
		mean = 0;
		count = 0;
		ki = "";

		recKiir(innen);

		ratlag(innen);
		mean /= (double) count;
		rszoras(innen);
		if (count - 1 > 0)
			var = Math.sqrt(var / (count - 1));
		else
			var = Math.sqrt(var);

		ki += ("depth = " + depth + "\n");
		ki += ("mean = " + mean + "\n");
		ki += ("var = " + var + "\n");

		System.out.print(ki);
		
		return ki;
	}
	
	public static void main(String[] args) {
		String be = "01111001001001000111";
    	Fa fa = new Fa();
        for(char c : be.toCharArray())
      	  fa.UjElem(c == '1');
        fa.kiir(fa.gyoker);
	}
}

class Elem {
	private boolean oldal;
	private Elem bal;
	private Elem jobb;
	private int melyseg;

	public Elem() {
		this.oldal = false;
		this.bal = null;
		this.jobb = null;
		this.melyseg = 0;
	}

	public Elem(boolean oldal, int melyseg) {
		this.oldal = oldal;
		this.bal = null;
		this.jobb = null;
		this.melyseg = melyseg;
	}

	public Elem BalOldal() {
		return this.bal;
	}

	public void BalOldal(Elem bal) {
		this.bal = bal;
	}

	public Elem JobbOldal() {
		return this.jobb;
	}

	public void JobbOldal(Elem jobb) {
		this.jobb = jobb;
	}

	public void EzBalOldal() {
		this.oldal = false;
	}

	public void EzJobbOldal() {
		this.oldal = true;
	}

	public boolean VanBalOldal() {
		return this.bal != null;
	}

	public boolean VanJobbOldal() {
		return this.jobb != null;
	}

	public int Melyseg() {
		return this.melyseg;
	}

	public boolean Oldal() {
		return this.oldal;
	}
}
