package hu.mark.bintree;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest 
    extends TestCase
{
	private Fa testFa;
	
    public AppTest( String testName )
    {
        super( testName );
        
        testFa = new Fa();
        
    	String be = "01111001001001000111";
        for(char c : be.toCharArray())
        	testFa.UjElem(c == '1');
        
        System.out.println(testFa.kiir());
    }

    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testApp()
    {
    	//Itt megnézi, hogy a bekért eredményre 4-et ad-e vissza.
    	assertEquals(4, testFa.getDepth());
    	
    	//Itt megnézi az átlagot, hogy 2.75-e.
    	assertEquals(2.75, testFa.getMean());
    	
    	//A szórás kerekítve van lefelé 2 tizedes jegyre.
    	assertEquals(0.95, Math.floor(testFa.getVar() * 100.0f) / 100.0f);
    }
}
