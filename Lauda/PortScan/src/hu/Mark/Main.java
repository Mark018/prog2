package hu.Mark;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

class OpenPorts {
	private ArrayList<Integer> ports;
	private int finished;
	private int max;

	public OpenPorts(int max) {
		this.ports = new ArrayList<Integer>();
		this.finished = 0;
		this.max = max;
	}

	public void clear() {
		this.ports.clear();
	}

	public void open(int port) {
		this.ports.add(port);
		this.next();
	}

	public ArrayList<Integer> getList() {
		return this.ports;
	}

	public void next() {
		this.finished++;
	}

	public boolean isDone() {
		return this.finished == this.max;
	}

	public int getMax() {
		return this.max;
	}
}

class PortScan extends Thread {
	private int port;
	private String ip;

	public PortScan(String ip, int port) {
		this.port = port;
		this.ip = ip;
		this.start();
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket(ip, port);
			socket.setSoTimeout(100);
			Main.getInstance().getPorts().open(port);
			socket.close();
		} catch (Exception e) {
			Main.getInstance().getPorts().next();
		}
	}
}

public class Main {
	private static Main instance;
	private OpenPorts ports;

	public Main() throws InterruptedException {
		instance = this;
		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		System.out.print("Adj egy IP-t: ");
		String ip = scan.next();
		System.out.print("Add meg a ranget-t: ");
		int range = Math.max(Math.min(scan.nextInt(), (int)Math.pow(256, 2)), 1);
		this.ports = new OpenPorts(range);

		for (int i = 0; i < this.ports.getMax(); ++i)
			new PortScan(ip, i);

		System.out.println("A rendszer picit dolgozik...");

		while (!this.ports.isDone())
			Thread.sleep(10);

		if (this.ports.getList().size() > 0) {
			System.out.println("Nyitott portok:");
			for (int port : this.ports.getList())
				System.out.println(port);
		} else {
			System.out.println("Nincs nyitott port.");
		}
	}

	public static Main getInstance() {
		return instance;
	}

	public OpenPorts getPorts() {
		return this.ports;
	}

	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			System.out.println("Nem.. " + e.getMessage());
		}
	}
}