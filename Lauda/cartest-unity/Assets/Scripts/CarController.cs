﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private float m_horizontalInput;
    private float m_verticalInput;
    private float m_steeringAngle;

    public WheelCollider frontRightW, frontLeftW;
    public WheelCollider rearRightW, rearLeftW;

    public Transform frontRightT, frontLeftT;
    public Transform rearRightT, rearLeftT;

    public float maxSteerAngle = 30;
    public float motorForce = 150;

    public void GetInput()
    {
        m_horizontalInput = Input.GetAxis("Horizontal");
        m_verticalInput = Input.GetAxis("Vertical");
    }

    private void Steer()
    {
        m_steeringAngle = maxSteerAngle * m_horizontalInput;
        frontRightW.steerAngle = m_steeringAngle;
        frontLeftW.steerAngle = m_steeringAngle;
    }

    private void Accelerate()
    {
        //frontRightW.motorTorque = m_verticalInput * motorForce;
        //frontLeftW.motorTorque = m_verticalInput * motorForce;

        rearRightW.motorTorque = m_verticalInput * motorForce;
        rearLeftW.motorTorque = m_verticalInput * motorForce;
    }

    private void UpdateWheelPoses()
    {
        UpdateWheelPose(frontRightW, frontRightT);
        UpdateWheelPose(frontLeftW, frontLeftT);
        UpdateWheelPose(rearRightW, rearRightT);
        UpdateWheelPose(rearLeftW, rearLeftT);
    }

    private void UpdateWheelPose(WheelCollider _collider, Transform _transform)
    {
        Vector3 _pos = _transform.position;
        Quaternion _quat = _transform.rotation;

        _collider.GetWorldPose(out _pos, out _quat);

        _transform.position = _pos;
        _transform.rotation = _quat;
    }

    private void FixedUpdate()
    {
        GetInput();
        Steer();
        Accelerate();
        UpdateWheelPoses();
    }
}
